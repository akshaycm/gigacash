
Debian
====================
This directory contains files used to package Gigacashd/Gigacash-qt
for Debian-based Linux systems. If you compile Gigacashd/Gigacash-qt yourself, there are some useful files here.

## Gigacash: URI support ##


Gigacash-qt.desktop  (Gnome / Open Desktop)
To install:

	sudo desktop-file-install Gigacash-qt.desktop
	sudo update-desktop-database

If you build yourself, you will either need to modify the paths in
the .desktop file or copy or symlink your Gigacashqt binary to `/usr/bin`
and the `../../share/pixmaps/Gigacash128.png` to `/usr/share/pixmaps`

Gigacash-qt.protocol (KDE)

