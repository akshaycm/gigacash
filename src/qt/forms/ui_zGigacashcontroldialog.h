/********************************************************************************
** Form generated from reading UI file 'zGigacashcontroldialog.ui'
**
** Created by: Qt User Interface Compiler version 5.5.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ZGCASHCASHCONTROLDIALOG_H
#define UI_ZGCASHCASHCONTROLDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QDialogButtonBox>
#include <QtWidgets/QFormLayout>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_ZGigacashControlDialog
{
public:
    QGridLayout *gridLayout;
    QFormLayout *formLayout;
    QLabel *labelQuantity;
    QLabel *labelQuantity_int;
    QLabel *labelZGigacash;
    QLabel *labelZGigacash_int;
    QPushButton *pushButtonAll;
    QVBoxLayout *verticalLayout;
    QTreeWidget *treeWidget;
    QDialogButtonBox *buttonBox;

    void setupUi(QDialog *ZGigacashControlDialog)
    {
        if (ZGigacashControlDialog->objectName().isEmpty())
            ZGigacashControlDialog->setObjectName(QStringLiteral("ZGigacashControlDialog"));
        ZGigacashControlDialog->resize(681, 384);
        ZGigacashControlDialog->setMinimumSize(QSize(681, 384));
        gridLayout = new QGridLayout(ZGigacashControlDialog);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        formLayout = new QFormLayout();
        formLayout->setObjectName(QStringLiteral("formLayout"));
        formLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        labelQuantity = new QLabel(ZGigacashControlDialog);
        labelQuantity->setObjectName(QStringLiteral("labelQuantity"));

        formLayout->setWidget(0, QFormLayout::LabelRole, labelQuantity);

        labelQuantity_int = new QLabel(ZGigacashControlDialog);
        labelQuantity_int->setObjectName(QStringLiteral("labelQuantity_int"));

        formLayout->setWidget(0, QFormLayout::FieldRole, labelQuantity_int);

        labelZGigacash = new QLabel(ZGigacashControlDialog);
        labelZGigacash->setObjectName(QStringLiteral("labelZGigacash"));

        formLayout->setWidget(1, QFormLayout::LabelRole, labelZGigacash);

        labelZGigacash_int = new QLabel(ZGigacashControlDialog);
        labelZGigacash_int->setObjectName(QStringLiteral("labelZGigacash_int"));

        formLayout->setWidget(1, QFormLayout::FieldRole, labelZGigacash_int);

        pushButtonAll = new QPushButton(ZGigacashControlDialog);
        pushButtonAll->setObjectName(QStringLiteral("pushButtonAll"));

        formLayout->setWidget(2, QFormLayout::LabelRole, pushButtonAll);


        gridLayout->addLayout(formLayout, 0, 0, 1, 1);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        treeWidget = new QTreeWidget(ZGigacashControlDialog);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setText(3, QStringLiteral("Confirmations"));
        __qtreewidgetitem->setText(2, QStringLiteral("zGigacash Public ID"));
        __qtreewidgetitem->setText(1, QStringLiteral("Denomination"));
        __qtreewidgetitem->setText(0, QStringLiteral("Select"));
        treeWidget->setHeaderItem(__qtreewidgetitem);
        treeWidget->setObjectName(QStringLiteral("treeWidget"));
        treeWidget->setMinimumSize(QSize(0, 250));
        treeWidget->setAlternatingRowColors(true);
        treeWidget->setSortingEnabled(true);
        treeWidget->setColumnCount(5);
        treeWidget->header()->setDefaultSectionSize(100);

        verticalLayout->addWidget(treeWidget);

        buttonBox = new QDialogButtonBox(ZGigacashControlDialog);
        buttonBox->setObjectName(QStringLiteral("buttonBox"));
        buttonBox->setOrientation(Qt::Horizontal);
        buttonBox->setStandardButtons(QDialogButtonBox::Ok);

        verticalLayout->addWidget(buttonBox);


        gridLayout->addLayout(verticalLayout, 1, 0, 1, 1);


        retranslateUi(ZGigacashControlDialog);
        QObject::connect(buttonBox, SIGNAL(accepted()), ZGigacashControlDialog, SLOT(accept()));
        QObject::connect(buttonBox, SIGNAL(rejected()), ZGigacashControlDialog, SLOT(reject()));

        QMetaObject::connectSlotsByName(ZGigacashControlDialog);
    } // setupUi

    void retranslateUi(QDialog *ZGigacashControlDialog)
    {
        ZGigacashControlDialog->setWindowTitle(QApplication::translate("ZGigacashControlDialog", "Select zGigacash to Spend", 0));
        labelQuantity->setText(QApplication::translate("ZGigacashControlDialog", "Quantity", 0));
        labelQuantity_int->setText(QApplication::translate("ZGigacashControlDialog", "0", 0));
        labelZGigacash->setText(QApplication::translate("ZGigacashControlDialog", "zGigacash", 0));
        labelZGigacash_int->setText(QApplication::translate("ZGigacashControlDialog", "0", 0));
        pushButtonAll->setText(QApplication::translate("ZGigacashControlDialog", "Select/Deselect All", 0));
        QTreeWidgetItem *___qtreewidgetitem = treeWidget->headerItem();
        ___qtreewidgetitem->setText(4, QApplication::translate("ZGigacashControlDialog", "Is Spendable", 0));
    } // retranslateUi

};

namespace Ui {
    class ZGigacashControlDialog: public Ui_ZGigacashControlDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ZGCASHCASHCONTROLDIALOG_H
